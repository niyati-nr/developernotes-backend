const BlogPost = require('../models/blogpost.model');
const express = require('express');
const { ReplSet } = require('mongodb');
const router = express.Router();
router.get('/',async (req,res) => {
    try{
        const posts = await BlogPost.find();
        res.json(posts);
    }catch(err){
        res.json({'message':err});
    }
});
router.get('/:id',async (req,res) => {
    try{
        const post = await BlogPost.findById(req.params.id);
        res.json(post);
    }catch(err){
        res.json({'message':err});
    }
});
router.delete('/:id',async (req,res) => {
    const id = req.params.id;
    try{
        const removedPost = await BlogPost.remove({'_id':id});
        res.json(removedPost);
    }catch(err){
        res.json({'message':err});
    }
});
router.patch('/:id',async (req,res) => {
    const id = req.params.id;
    const details = {'_id':id};
    try{
        const updatedPost = await BlogPost.updateOne(
            details,
            {$set:{
                    title: req.body.title,
                    content: req.body.content,
                    author: req.body.author,
                    categories:req.body.categories,
                    authorid:req.body.authorid,
                    bloguserid:req.body.bloguserid,
                    date:Date.now()
                }
            }
        );
        res.json(updatedPost);
    }
    catch(err){
        res.json({'message':err});
    }
});
router.post('/',async (req,res) => {
    const Post = new BlogPost({
        title: req.body.title,
        content: req.body.content,
        author: req.body.author,
        categories:req.body.categories,
        authorid:req.body.authorid,
        bloguserid:req.body.bloguserid
    });
    try{
        const savedPost = await Post.save();
        res.json(savedPost);
    } catch(err){
        res.json({'message':err});
    }
});
module.exports = router;