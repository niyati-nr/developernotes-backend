require('dotenv/config')
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const cookieParser=require('cookie-parser');

const app = express();

var corsOptions = {
    origin: '*'
  }

app.use(cookieParser());
app.use(cors(corsOptions));
app.use(express.json())

const port = process.env.PORT || 8085;
const postsRoute = require('./app/routes/devnotes_routes');
const categoriesRoute = require('./app/routes/category_routes');
const usersRoute = require('./app/routes/user_routes');


app.use('/devpost',postsRoute);
app.use('/categories',categoriesRoute);
app.use('/users',usersRoute);


app.get('/',(req,res) => {
    res.send('Hello');
})
mongoose.connect(
    process.env.DB_URL,
    {useNewUrlParser: true, useUnifiedTopology: true},
    () => console.log("Welcome to DevNotes. We are live on port number "+port)
);
app.listen(port);