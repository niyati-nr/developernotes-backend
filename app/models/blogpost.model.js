const mongoose = require('mongoose');

const BlogPostSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    content:{
        type: String,
        required: true
    },
    date:{
        type:String,
        default:Date.now
    },
    categories:{
        type:[String],
        required:true,
        default:['general']
    },
    author:{
        type:String,
        required:true
    },
    authorid:{
        type:String,
        default:'anonymous'
    },
    bloguserid:{
        type:String,
        default:'anonymous'
    }
})

module.exports = mongoose.model('BlogPosts', BlogPostSchema);